package atm.server;

import java.util.Map;
import java.util.WeakHashMap;

/**
 * Created by IntelliJ IDEA.
 * User: shesdmi
 * Date: 3/7/13
 * Time: 5:18 PM
 * To change this template use File | Settings | File Templates.
 */
public class StorageService {

    private Map<String, Account> accountHashMap = new WeakHashMap<>();
    private Map<String, Session> sessionHashMap = new WeakHashMap<>();

    public synchronized Session createSessionById(String sessionId, String userId, String sourceId, byte[] credentials) {

        Session session = new Session(getOrCreateAccount(userId), sessionId, sourceId, credentials);

        sessionHashMap.put(sessionId, session);
        return session;
    }

    public synchronized void closeSessionById(String sessionId) {
        sessionHashMap.remove(sessionId);
    }

    private synchronized Account getOrCreateAccount(String accountId) {
        Account res = null;

            res = accountHashMap.get(accountId);

            if(res == null) {
              res = new Account(accountId);
              accountHashMap.put(accountId, res);
            }


        return res;

    }
    public synchronized Session lookupSession(String sessionId) {
        return sessionHashMap.get(sessionId);
    }

    public  Session lookupSessionProxyForAccount(String accountId) {
        return new Session(getOrCreateAccount(accountId), null, null, null);

    }

}
